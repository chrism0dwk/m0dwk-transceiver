/*
 * systemtime.hpp
 *
 *  Created on: 18/12/2014
 *      Author: cpjewell
 */

#ifndef SYSTEMTIME_HPP_
#define SYSTEMTIME_HPP_

#ifndef F_CPU
  #error "F_CPU must be defined"
#endif

#include <avr/interrupt.h>

static volatile uint32_t msec;
static bool isInit;

void
initSystemTime()
{
  if (!isInit)
    {
      // Freq scaling
      TCCR1B |= _BV(CS10) | _BV(CS11); // Divide clk freq by 64
      OCR1A = F_CPU / 64000; // Clock freq / 64 / 1000
      // CLEAR OCF1A
      TIFR |= _BV(OCF1A);
      TIMSK |= _BV(OCIE1A);

      isInit = true;
    }
}

uint32_t
getSystemTime()
{
  return msec;
}


ISR(TIMER1_COMPA_vect)
{
  // Reset counter
  TCNT1 = 0;
  msec++;
}

#endif /* SYSTEMTIME_HPP_ */
