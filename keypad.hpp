/*
 * keypad.h
 *
 *  Created on: 9/12/2014
 *      Author: cpjewell
 */

#ifndef KEYPAD_HPP_
#define KEYPAD_HPP_

#include <stdint.h>
#include <stdlib.h>
#include "avrio.hpp"
#include "systemtime.hpp"

static const char keys4x3[] = {'1','2','3','4','5','6','7','8','9','*','0','#'};

static void
defaultEventHandler(const char key)
{
  return;
}




template <Pin COL0, Pin COL1, Pin COL2,
          Pin ROW0, Pin ROW1, Pin ROW2, Pin ROW3,
          uint16_t SHORTPRESS=50, uint16_t LONGPRESS=2000>
class Keypad4x3
{
  public:
  typedef void (*KeypadEventHandler)(const char key);

  Keypad4x3() : colddr_(0), rowddr_(0), colport_(0), rowport_(0),
                timestamp_(getSystemTime()), isKeyPressed_(false), lastKey_('\0'),
                onLongPress_(defaultEventHandler), onShortPress_(defaultEventHandler)
  {

  };

  void
  listen()
  {
    char key = GetKey();
      if (key != '\0')
        {
          lastKey_ = key;

          if (!isKeyPressed_)
            { // If key has just gone down...
              timestamp_ = getSystemTime();
              isKeyPressed_ = true;
            }
        }
      else if (isKeyPressed_)
        { // Key has just been released
          if (getSystemTime() - timestamp_ > LONGPRESS) // Long press
            {
              (*onLongPress_)(lastKey_);
            }
          else if(getSystemTime() - timestamp_ > SHORTPRESS) // Short press
            {  // Short press => Program mode
              (*onShortPress_)(lastKey_);
            }
          isKeyPressed_ = false;
        }
  }

  void
  onLongPress(KeypadEventHandler handler)
  {
    onLongPress_ = handler;
  }

  void
  unLongPress()
  {
    onLongPress_ = defaultEventHandler;
  }

  void
  onShortPress(KeypadEventHandler handler)
  {
    onShortPress_ = handler;
  }

  void
  unShortPress()
  {
    onShortPress_ = defaultEventHandler;
  }

  private:
  //! Returns a key value if pressed
  char
  GetKey()
  {
    // Save pins
    PushPins();

    // Set DDR to what we require
    IO::Input<ROW0, ROW1, ROW2, ROW3>();
    IO::High<ROW0, ROW1, ROW2, ROW3>(); // Pull up resistors on inputs
    IO::Low<COL0, COL1, COL2>(); // Take cols low
    IO::Output<COL0, COL1, COL2>();  // Tri-state columns
    _delay_us(10);

    uint8_t buff = IO::Get<(PortID)IO::Portof(ROW0)>(); // See if any rows are low, invert bits!
    _delay_ms(3);
    uint8_t buff1 = IO::Get<(PortID)IO::Portof(ROW0)>();
    if(buff != buff1) {
        PopPins();
        return '\0';  // Bounce -- false alarm
    }

    buff = ~buff;
    if(!(buff & Rowmask())) {
       PopPins();
       return '\0'; // If no rows are low, return immediately -- no key pressed
     }

    // Else detect which row was pressed
    uint8_t row; uint8_t rowpins[] = {ROW0, ROW1, ROW2, ROW3};
    if(buff & _BV(ROW0 % 8)) row = 0;
    else if(buff & _BV(ROW1 % 8)) row = 1;
    else if(buff & _BV(ROW2 % 8)) row = 2;
    else row = 3;

    // Cycle cols to detect column
    uint8_t col = 0;
    IO::Input<COL0, COL2>();  // Tri-state to avoid a short if two keys pressed
    _delay_us(10);
    if( (~IO::Get<(PortID)IO::Portof(ROW0)>() & _BV(rowpins[row] % 8)) == _BV(rowpins[row] % 8) ) col = 1;
    IO::Input<COL1>(); IO::Output<COL2>();
    _delay_us(10);
    if( (~IO::Get<(PortID)IO::Portof(ROW0)>() & _BV(rowpins[row] % 8)) == _BV(rowpins[row] % 8) ) col = 2;

    // Restore ddr and port
    PopPins();
    _delay_us(10);
    return keys4x3[row*3 + col];
  }


  constexpr
  uint8_t Colmask()
  {
    return _BV(COL0 % 8) | _BV(COL1 % 8) | _BV(COL2 % 8);
  }
  constexpr
  uint8_t Rowmask()
  {
    return _BV(ROW0 % 8) | _BV(ROW1 % 8) | _BV(ROW2 % 8) | _BV(ROW3 % 8);
  }

  void
  PushPins()
  {
    IO::DDR ddr;
    IO::Port port;
    colport_ = port(IO::Portof(COL0));
    rowport_ = port(IO::Portof(ROW0));
    colddr_ = ddr(IO::Portof(COL0));
    rowddr_ = ddr(IO::Portof(ROW0));
  }

  void
  PopPins()
  {
    IO::DDR ddr;
    IO::Port port;
    ddr(IO::Portof(COL0)) = colddr_;
    ddr(IO::Portof(ROW0)) = rowddr_;
    port(IO::Portof(COL0)) = colport_;
    port(IO::Portof(ROW0)) = rowport_;
  }

  uint8_t colddr_, rowddr_;
  uint8_t colport_, rowport_;
  uint32_t timestamp_;
  bool isKeyPressed_;
  char lastKey_;
  KeypadEventHandler onLongPress_;
  KeypadEventHandler onShortPress_;
};



#endif /* KEYPAD_HPP_ */
