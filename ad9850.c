/*
 * ad9850.c
 *
 *  Created on: 30/11/2014
 *      Author: cpjewell
 */


#include <util/delay.h>
#include "ad9850.h"

// Pulses a pin on the DDS port
static
void
pulseFreqUpd()
{
  DDSPORT |= _BV(DDSFUPD);
  _delay_us(4);
  DDSPORT &= ~_BV(DDSFUPD);
}

static
void
pulseWCLK()
{
  DDSPORT |= _BV(DDSWCLK); // Word clock high
  asm volatile ("nop");
  DDSPORT &= ~_BV(DDSWCLK); // Word clock low
}

static
void
reset()
{
  DDSPORT |= _BV(DDSRST);
  _delay_us(4);
  DDSPORT &= ~_BV(DDSRST);
}


// Sends a byte to the DDS in serial mode
static
void
ddsSendByte(char byte)
{
  for(int i=8; i>0; --i)
    {
      if(byte & 0x01)
        DDSPORT |= _BV(DDSDATA); // Data on pin
      else
        DDSPORT &= ~_BV(DDSDATA);
      pulseWCLK();
      byte >>= 1;
    }
}




// Initialises the DDS
void
ddsInit()
{
  // DDR
  DDSDDR |= _BV(DDSDATA) | _BV(DDSWCLK) | _BV(DDSFUPD) | _BV(DDSRST);

  // Clear pins
  DDSPORT &= ~(_BV(DDSDATA) | _BV(DDSWCLK) | _BV(DDSFUPD) | _BV(DDSRST));

  // Reset
  reset();

  // Pulse WCLK
  pulseWCLK();

  // Pulse freq pin to put into serial mode
  pulseFreqUpd();
  _delay_us(4);
  pulseFreqUpd();
}


void
ddsLoadFreq(const uint32_t freq)
{
  uint32_t tword = freq * DDSTUNECONST;

  DDSPORT &= ~(_BV(DDSDATA) | _BV(DDSWCLK) | _BV(DDSFUPD));

  // Send tuning word
  char* byte = (char*)&tword;
  for(int i=0; i<4; ++i)
    ddsSendByte(byte[i]);

  // Command/Power/Phase
  ddsSendByte(0x00);

  // Update freq
  pulseFreqUpd();

  // Done
}


