/*
 * lcd.h
 *
 *  Created on: 18/11/2014
 *      Author: cpjewell
 */

#ifndef LCD_H_
#define LCD_H_

#include <util/delay.h>
#include "avrio.hpp"

/* The following constants must be
 * specified as template parameters:
 * DA -- LSB data pin on data port (must be consecutive!)
 * Control pins:
 * RS -- register select pin
 * RW -- read/write pin
 * E -- enable pin
 */

template <Pin DA, Pin DB, Pin DC, Pin DD, Pin RS, Pin RW, Pin E>
class Lcd
{
public:
  Lcd()
    { static_assert((DA % 8) < 5, "DA bit must be <= 4!");
      Initialize();
    };

  void PutChar(char c) const
  {
    IO::Output<DA,DB, DC, DD>(); // Data direction out
    IO::Low<RW>(); // Select data register
    IO::High<RS>();
    SetDataPins(c);  // Put high byte on port

    IO::High<E>();
    asm volatile ("nop");
    asm volatile ("nop");
    IO::Low<E>();

    // Swap c bits
    asm volatile("swap %0" : "=r" (c) : "0" (c));
    SetDataPins(c);

    IO::High<E>();
    asm volatile ("nop");
    asm volatile("nop");
    IO::Low<E>();

    Wait();
  }

  void SetCursorPos(const uint8_t row, const uint8_t col) const
  {
    if(row==0 && col==0) Cmd4(0x02);
    else {
      uint8_t addr = row*0x40 + col;
      addr |= _BV(7);
      Cmd4(addr);
    }
    Wait();
  }

  //! Write string to LCD
  //!
  //! Write a char string @param str
  //!  of length @param len to LCD
  void WriteString(const char* str, const uint8_t len) const
  {
    for(uint8_t i=0; i<len; ++i)
      {
        if(str[i]=='\0') break;
        PutChar(str[i]);
      }
  }


private:
  void
  Initialize() const {
    //setup the LCD control signals on PortC
    IO::Output<RS,RW,E>();
    IO::Low<RS,RW,E>();

    //if called right after power-up, we'll have to wait a bit (fine-tune for faster execution)
    _delay_ms(100);
    //tell the LCD that it's used in 8-bit mode, 5x7 font, 2 lines
    Cmd8(0b00110000); //N=1, F=0
    _delay_ms(5);
    Cmd8(0b00110000);
    _delay_ms(5);
    Cmd8(0b00110000);
    _delay_ms(5);
    Cmd8(0b00100000);  // Set to 4-bit
    _delay_ms(5);
    // 2 lines, 5*8 font, 4 bit mode
    Cmd4(0b00101100);
    _delay_ms(5);//Wait();
    //display on, cursor on (blinking)
    Cmd4(0b00001111);
    _delay_ms(5);//Wait();
    //now Low the display, cursor home
    Cmd4(0b00000001);
    _delay_ms(5);//Wait();
    //cursor auto-inc
    Cmd4(0b00000010);
    _delay_ms(5);//Wait();
  }

  // Sets pins according to high nibble
  void
  SetDataPins(char c) const
  {
    c &= 0xF0;  // Bit mask
    c >>= 4-(DA % 8); // Right shift for DA != 4

    volatile char tmp = IO::Get<(PortID)IO::Portof(DA)>();
    tmp &= ~( 0xF0 >> (4-DA%8)); // Clear high nibble
    // Put data on port
    IO::Put<(PortID)IO::Portof(DA)>(tmp | c);
  }

  // High nibble contains data
  char
  GetDataPins() const
  {
    char tmp = IO::Get<(PortID)IO::Portof(DA)>();
    tmp <<= 4-(DA % 8);
    return tmp & 0xF0;
  }

  //! 8-bit command
  void Cmd8(const uint8_t cmd) const
  {
    IO::Output<DA,DB,DC,DD>();   // Data direction out
    IO::Low<RS,RW,E>();
    SetDataPins(cmd);
    //IO::Put<PortID::B>(cmd);
    IO::High<E>(); //  Set E high
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    IO::Low<E,RS,RW>(); // Low E
  }
  //! 4-bit command sequence
  // Cmd contained in the low nibble!
  void Cmd4(uint8_t cmd) const
  {
    IO::Output<DA,DB,DC,DD>();
    IO::Low<RS,RW,E>();
    SetDataPins(cmd);
    //IO::Put<PortID::B>(cmd);
    IO::High<E>();
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    IO::Low<E>();
    asm volatile ("nop");
    asm volatile("swap %0" : "=r" (cmd) : "0" (cmd));
    SetDataPins(cmd);
    //IO::Put<PortID::B>(cmd);
    IO::High<E>();
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    IO::Low<E>();

  }
  void Wait() const
  {
    while((GetAddr() & 0x80) == 0x80);
  }
  uint8_t
  GetAddr() const
  {
    IO::Input<DA, DB, DC, DD>(); // Input pins
    IO::Low<RS>();
    IO::High<RW>();
    IO::High<E>();
    asm volatile("nop");
    asm volatile("nop");
    volatile uint8_t nibbleH = GetDataPins();
    IO::Low<E>();
    asm volatile("nop");
    asm volatile("nop");
    IO::High<E>();
    asm volatile("nop");
    asm volatile("nop");
    volatile uint8_t nibbleL = GetDataPins();
    IO::Low<RW, E>();
    asm volatile("nop");
    IO::Output<DA, DB, DC, DD>();
    asm volatile("swap %0" : "=r" (nibbleL) : "0" (nibbleL));
    nibbleH |= nibbleL;
    return nibbleH;
  }

};




#endif /* LCD_H_ */
