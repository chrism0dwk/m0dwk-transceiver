# Makefile for AVR microcontrollers

PROJECT = ddsvfo

# Device information
AVRDEVICE = atmega8
AVRTARGETFREQUENCY = 8000000

# Programmer info
PORT = /dev/usb0
AVRDUDEOPTIONS = -pnull -cavrisp2 -Pusb -u
AVRDUDEACTIONOPTIONS = -Uflash:2:${PROJECT}.hex:a -Ulfuse:2:0x24:m -Uhfuse:w:0xd9:m


# Build variables

CPPFLAGS = -Wall -Os -mmcu=${AVRDEVICE} -DF_CPU=${AVRTARGETFREQUENCY}
CFLAGS = ${FLAGS} -std=gnu99
CXXFLAGS = ${FLAGS} -std=c++11
LDFLAGS = -Wl,-Map,${PROJECT}.map,--cref -mrelax -Wl,--gc-sections -mmcu=${AVRDEVICE}
OBJ = ddsvfo.o encoder.o

# Rules

all: ${PROJECT}.elf ${PROJECT}.hex ${PROJECT}.eep ${PROJECT}.lss sizedummy

%.o: %.cpp
	avr-g++ ${CPPFLAGS} ${CXXFLAGS} -o $@ -c $<

%.o: %.c
	avr-gcc ${CPPFLAGS} ${CFLAGS} -o $@ -c $<

%.elf: ${OBJ}
	avr-g++ ${LDFLAGS} -o $@ $^

%.lss: %.elf
	avr-objdump -h -S $< > ${PROJECT}.lss

%.hex: %.elf
	avr-objcopy -R .eeprom -R .fuse -R .lock -R.signature -O ihex $< $@

%.eep: %.elf
	avr-objcopy -j .eeprom --no-change-warnings --change-section-lma .eeprom=0 -O ihex $< $@

sizedummy: ${PROJECT}.elf
	avr-size --format=avr --mcu=${AVRDEVICE} $<

upload:
	avrdude ${AVRDUDEOPTIONS} ${AVRDUDEACTIONOPTIONS}

.PHONY: clean
clean:
	rm -f *.o *.elf *.hex
