/*
 * encoder.h
 *
 *  Created on: 18/11/2014
 *      Author: cpjewell
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#ifndef PORT_ENCODER
#define PORT_ENCODER     PORTD
#endif

#ifndef PIN_ENCODER
#define PIN_ENCODER      PIND
#endif

#ifndef DDR_ENCODER
#define DDR_ENCODER      DDRD
#endif

#ifndef ENCODER_A
#define ENCODER_A        2
#endif

#ifndef ENCODER_B
#define ENCODER_B        4
#endif

#ifndef ENCODER_BUTTON
#define ENCODER_BUTTON   3
#endif


typedef enum {
  ENCODER_NULL,
  ENCODER_RIGHT,
  ENCODER_LEFT
} encoderDir_t;

typedef enum {
  ENCODER_BTN_IDLE,
  ENCODER_BTN_PRESSED,
  ENCODER_BTN_DOWN,
  ENCODER_BTN_UP
} encoderBtn_t;

void
encoderInit();

encoderDir_t encoderIntDir();
uint8_t encoderButtonPressed();



#endif /* ENCODER_H_ */
