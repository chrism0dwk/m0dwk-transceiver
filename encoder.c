/*
 * encoder.c
 *
 *  Created on: 18/11/2014
 *      Author: cpjewell
 */


#include <avr/io.h>
#include <util/delay.h>

#include "encoder.h"



void
encoderInit()
{
  // Set DDR for encoder pins input (ie LOW)
  DDR_ENCODER &= ~(_BV(ENCODER_A)|_BV(ENCODER_B)|_BV(ENCODER_BUTTON));

  // Set internal pullups
  PORT_ENCODER |= (_BV(ENCODER_A)|_BV(ENCODER_B)|_BV(ENCODER_BUTTON));
}


// Code called from an interrupt generated
// by pin A.  We confirm the state of A,
// and return based on the state of B.
encoderDir_t
encoderIntDir()
{
  const uint8_t portmask = _BV(ENCODER_A) | _BV(ENCODER_B);
  const uint8_t leftmask = _BV(ENCODER_A);

  volatile uint8_t pins = PIN_ENCODER;
  _delay_ms(1);
  volatile uint8_t pin1 = PIN_ENCODER;
  if((pins & pin1 & portmask)==portmask)
    return ENCODER_RIGHT;
  else if ((pins & pin1 & portmask)==leftmask)
    return ENCODER_LEFT;
  else return ENCODER_NULL;
}


uint8_t
encoderButtonPressed()
{
  return (PORT_ENCODER & 0x04)==0x04;
}
