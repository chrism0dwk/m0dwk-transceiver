/* Name: ddsvfo.c
 * Author: Chris Jewell <chrism0dwk@gmail.com>
 * Created: 3/11/2014
 * Purpose: AVR-based VFO coupled to AD9850 and LCD
 */

// CPU Frequency
#ifndef F_CPU
  #error "F_CPU not defined"
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>

#include "lcd.hpp"
#include "ad9851.hpp"
#include "keypad.hpp"
#include "Shift24.hpp"
#include "systemtime.hpp"

extern "C"
{
#include "encoder.h"
}

// Global constants


// Operating frequency tuning
#define SYSTEM_MAXFREQ 40000000UL // Max receiver/transceiver opfreq
#define SYSTEM_MINFREQ 1000000UL  // 1MHz minimum freq
#define SYSTEM_FREQDISPLEN 8U     // Length of freq display, starting at 10s of MHz

static volatile uint32_t opfreq = 3600000;   // Operating freq
static volatile uint32_t offset;             // Tuning offset (for heterodyning)
static const uint32_t freqincr = 100;       // Tuning increment
static char kpdBuff[SYSTEM_FREQDISPLEN + 1];
static uint8_t kpdStrLen;

// System status register
static volatile uint8_t sysreg;
enum
{  // Bits
  SYSREG_DDSUPD = 0,  // Set if require DDS update
  SYSREG_LCDFUPD = 1, // Set if require freq display update
  SYSREG_LCDSUPD = 2, // Set if require state display update
  SYSREG_LCKFREQ = 3, // If set, frequency is locked
  SYSREG_FQPRGM = 4, // If set, system is in freq "program" mode
};

// LCD
typedef Lcd<Pin::B3, Pin::B4, Pin::B5, Pin::B6, Pin::B0, Pin::B1, Pin::B2> Lcd0;
const Lcd0 lcd0;

// AD9850 DDS module
typedef AD9851<Pin::C0, Pin::C1, Pin::C2, Pin::C3, 30000000> DDS0;

// Keypad
typedef Keypad4x3<Pin::D5, Pin::D6, Pin::D7, Pin::B3, Pin::B4, Pin::B5, Pin::B6> Keypad0;

// Shift register
typedef Shift24<Pin::D6,Pin::D5,Pin::D3> ShiftReg0;


static
void
displayFreq(const Lcd0& lcd, const uint32_t freq)
{
  lcd.SetCursorPos(0, 0); // Cursor to 00H
  char ascfreq[11]; // Need 6 digits accuracy

  ultoa(freq, ascfreq, 10);
  char* strpos;
  if (freq < 10000000)
    {
      lcd.WriteString(" ", 1);
      lcd.WriteString(ascfreq, 1);
      strpos = ascfreq + 1;
    }
  else
    {
      lcd.WriteString(ascfreq, 2);
      strpos = ascfreq + 2;
    }

  lcd.WriteString(".", 1);
  lcd.WriteString(strpos, 5);
}

static
void
displayFqPrgm()
{
  lcd0.SetCursorPos(0, 0);
  lcd0.WriteString(kpdBuff, 2);
  if (kpdStrLen > 1)
    lcd0.SetCursorPos(0, 3);
  lcd0.WriteString(kpdBuff + 2, 7);

}

static
void
setFreq(uint32_t freq)
{
  if (freq < SYSTEM_MINFREQ)
    opfreq = SYSTEM_MINFREQ;
  else if (opfreq > SYSTEM_MAXFREQ)
    opfreq = SYSTEM_MAXFREQ;
  else
    opfreq = freq;

  sysreg |= _BV(SYSREG_DDSUPD) | _BV(SYSREG_LCDFUPD);
}

// Changes the frequency of the DDS
// according to tuning speed.
static volatile uint32_t tuneTimestamp;
static
void
incrementFreq(int32_t incr)
{
  uint32_t timediff = getSystemTime() - tuneTimestamp;
  if (timediff < 20)
    incr *= 1000L;
  else if (timediff < 40)
    incr *= 100L;
  else if (timediff < 160)
    incr *= 10L;

  setFreq(opfreq + incr);
  tuneTimestamp = getSystemTime();
}

////////////////////////
// Encoder interrupts //
////////////////////////

// Set for rising edge of encoder A
ISR(INT0_vect)
{
  switch (encoderIntDir())
    {
  case ENCODER_LEFT:
    incrementFreq(-freqincr);
    break;
  case ENCODER_RIGHT:
    incrementFreq(freqincr);
    break;
  default:
    break;
    }
  sysreg &= ~_BV(SYSREG_FQPRGM);
}
// Initialises encoder interrupt
static
void
InitEncInt()
{
  // Rising edge of INT1 generates interrupt.
  MCUCR |= _BV(ISC01) | _BV(ISC00);

  // Clear interrupt
  //GIFR |= _BV(INTF0);

  // Turn on interrupt
  GICR |= _BV(INT0);
}

///////////
// Tasks //
///////////
static
void
updateDDS(DDS0 dds)
{
  if (sysreg & _BV(SYSREG_DDSUPD))
    {
      dds.SetFrequency(opfreq);
      sysreg &= ~_BV(SYSREG_DDSUPD);
    }
}

static
void
updateLCD(const Lcd0& lcd)
{
  cli();
  if (sysreg & _BV(SYSREG_LCDFUPD))
    {
      if (sysreg & _BV(SYSREG_FQPRGM))
        { // Freq entry
          displayFqPrgm();
        }
      else
        { // Normal display
          displayFreq(lcd, opfreq + offset);
        }
    }
  sysreg &= ~_BV(SYSREG_LCDFUPD);
  sei();
}

static
void
beginFqPrgm()
{
  lcd0.SetCursorPos(0, 0);
  lcd0.WriteString("  .     ", 8);
  lcd0.SetCursorPos(0, 0);

  // Zero out the keypad buffer
  memset(kpdBuff, 0, 9);
  kpdStrLen = 0;

  sysreg |= _BV(SYSREG_FQPRGM);
}

static
void
endFqPrgm()
{
  // Translate ASCII freq into uint32_t
  kpdBuff[SYSTEM_FREQDISPLEN - 1] = '0'; // Fill in the digit we've missed
  uint32_t f = strtoul(kpdBuff, NULL, 10);
  setFreq(f - offset);

  sysreg &= ~_BV(SYSREG_FQPRGM);
}

void
KpdShortPress(const char key)
{
  if ((sysreg & _BV(SYSREG_FQPRGM)) == 0)
    {
      beginFqPrgm();
    }
  if (key != '*' and key != '#')
    {
      kpdBuff[kpdStrLen] = key;
      kpdStrLen++;
    }
  if (kpdStrLen == SYSTEM_FREQDISPLEN - 1) // End if keypad buffer is full
    {
      endFqPrgm();
    }
  sysreg |= _BV(SYSREG_LCDFUPD);
}

int
main(void)
{
  cli();
  initSystemTime();
  _delay_ms(500);
  encoderInit();
  InitEncInt();  // Enable encoder interrupt

  DDS0 dds0(1.024);
  dds0.SetFrequency(opfreq);

  displayFreq(lcd0, opfreq + offset);
  lcd0.SetCursorPos(1, 0);
  lcd0.WriteString("T:F  MHz", 8);

  Keypad0 keypad0;
  keypad0.onShortPress(KpdShortPress);

  ShiftReg0 shift0;

  _delay_ms(1000);
  // Enable interrupts and enter event loop
  sei();
  while (1)
    {
      updateLCD(lcd0);
      updateDDS(dds0);
      keypad0.listen();
    };
}

