# Transceiver controller firmware for M0DWK Transceiver

This code targets the ATmega8 microcontroller, with an AD8991 direct digital synthesis chip, tuning knob (optical encoder), keypad (4x3 for direct frequency entry), LCD display, and a 24bit shift register for front panel status LEDs.

The code is based on C++ and uses extensive templating to push all constant-based decisions (pin connections, frequencies, etc) into compile time, rather than runtime code bloat.  The result is a small binary (c. 4K) for uploading to the microcontroller.

The `main()` function is contained in `ddsvfo.cpp`.  The project `Makefile` will build the binary image by default, e.g.

```bash
$ cd m0dwk-transceiver
$ make
```

and is configured to use an AVRISP2 in-situ programmer connected via USB to the computer, e.g.
```bash
$ make upload
```

Edit the `Makefile` macros for your own situation.
