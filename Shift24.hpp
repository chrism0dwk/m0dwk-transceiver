/*
 * Shift24.hpp
 *
 *  Created on: 29/12/2014
 *      Author: cpjewell
 *     Purpose: Implements a 24 bit shift register made from
 *              3 cascaded 4094 8 bit shift registers.
 */

#include <util/delay.h>
#include "avrio.hpp"

#ifndef SHIFT24_HPP_
#define SHIFT24_HPP_

template<Pin DATA, Pin CLK, Pin LATCH>
class Shift24
{
public:

  Shift24() {
    ClearPins();
    RollPins();
  }

  void
  ClearPins()
  {
    char zeros[] = {0,0,0};
    PushData(zeros);
  }

  void
  RollPins()
  {
    // Roll all pins
    uint32_t blink = 0x1;
    for(uint8_t i = 0; i<24; ++i) {
        PushData((char*)&blink);
        blink <<= 1;
        _delay_ms(100);
    }
  }

  void
  PushData(const char* data)
    {
      PushPins();
      IO::Output<DATA,CLK,LATCH>();
      IO::Low<DATA,CLK,LATCH>();
      //_delay_us(50);
      // We expect data to be 3 bytes long
      for(uint8_t i = 0; i < 3; ++i)
        {
          char byte = data[i];
          for(uint8_t bit = 0; bit < 8; ++bit)
            {
              if(byte & 0x01) IO::High<DATA>();
              else IO::Low<DATA>();
              //_delay_us(50);
              IO::High<CLK>();
              asm volatile ("nop");
              IO::Low<CLK>();
              //_delay_us(50);
              byte >>= 1;
            }
        }
      IO::High<LATCH>();
      asm volatile ("nop");
      IO::Low<LATCH>();
      PopPins();
    }

private:
  void
  PushPins()
  {
    IO::DDR ddr;
    IO::Port port;
    dataport_ = port(IO::Portof(DATA));
    dataddr_ = ddr(IO::Portof(DATA));
    clkport_ = port(IO::Portof(CLK));
    clkddr_ = ddr(IO::Portof(CLK));
    latchport_ = port(IO::Portof(LATCH));
    latchddr_ = ddr(IO::Portof(LATCH));
    _delay_us(10);
  }

  void
  PopPins()
  {
    IO::DDR ddr;
    IO::Port port;
    port(IO::Portof(DATA)) = dataport_;
    ddr(IO::Portof(DATA)) = dataddr_;
    port(IO::Portof(CLK)) = clkport_;
    ddr(IO::Portof(CLK)) = clkddr_;
    port(IO::Portof(LATCH)) = latchport_;
    ddr(IO::Portof(LATCH)) = latchddr_;
    _delay_us(10);
  }

  uint8_t dataport_, dataddr_;
  uint8_t clkport_, clkddr_;
  uint8_t latchport_, latchddr_;
};



#endif /* SHIFT24_HPP_ */
