/*
 * ad9850.h
 *
 *  Created on: 30/11/2014
 *      Author: cpjewell
 */

#ifndef AD9850_H_
#define AD9850_H_

#include <avr/interrupt.h>
#include "avrio.hpp"


#define DDSTUNECONST  (0xFFFFFFFF / 125000000)


template<Pin data, Pin wclk, Pin fupd, Pin reset, uint32_t oscfreq>
class AD9850
{
public:
  void
  SetFrequency(uint32_t freq) const
  {
    uint32_t tword = freq * (0xFFFFFFFF / oscfreq);

    cli(); // Prevent interrupts screwing freq upload

    IO::Output<data, wclk, fupd>();
    IO::Low<data,wclk,fupd>();

    // Send tuning word
    char* byte = (char*)&tword;
    for(int i=0; i<4; ++i)
      SendByte(byte[i]);

    // Command/Power/Phase
    SendByte(0x00);

    // Update freq
    PulseFreqUpd();

    sei(); // Re-enable interrupts
  }

private:
  void
  PulseFreqUpd() const
  {
    IO::High<fupd>();
    _delay_us(4);
    IO::Low<fupd>();
  }

  void
  PulseWCLK() const
  {
    IO::High<wclk>(); // Word clock high
    asm volatile ("nop");
    IO::Low<wclk>(); // Word clock low
  }

  void
  Reset() const
  {
    IO::High<reset>();
    _delay_us(4);
    IO::Low<reset>();
  }


  // Sends a byte to the DDS in serial mode
  void
  SendByte(char byte) const
  {
    for(int i=8; i>0; --i)
      {
        if(byte & 0x01)
          IO::High<data>();
        else
          IO::Low<data>();
        PulseWCLK();
        byte >>= 1;
      }
  }




  // Initialises the DDS
  void
  Init()
  {

    // Reset
    Reset();

    // Pulse WCLK
    PulseWCLK();

    // Pulse freq pin to put into serial mode
    PulseFreqUpd();
    _delay_us(4);
    PulseFreqUpd();
  }



};


#endif /* AD9850_H_ */
