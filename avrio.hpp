/*
 * avrcppio.h
 *
 *  Created on: 10/12/2014
 *      Author: cpjewell
 */

#ifndef AVRCPPIO_HPP_
#define AVRCPPIO_HPP_

#include <avr/io.h>

typedef enum
{
#ifdef PORTA
  A,
#endif
#ifdef PORTB
  B,
#endif
#ifdef PORTC
  C,
#endif
#ifdef PORTD
  D,
#endif
#ifdef PORTE
E,
#endif
#ifdef PORTF
F,
#endif
#ifdef PORTG
G,
#endif
#ifdef PORTH
H,
#endif
#ifdef PORTJ
J,
#endif
#ifdef PORTK
K,
#endif
#ifdef PORTL
L,
#endif
} PortID;

static volatile uint8_t* const portAddr[] =
{
#ifdef PORTA
  &PORTA,
#endif
#ifdef PORTB
  &PORTB,
#endif
#ifdef PORTC
  &PORTC,
#endif
#ifdef PORTD
  &PORTD,
#endif
#ifdef PORTE
&PORTE,
#endif
#ifdef PORTF
&PORTF,
#endif
#ifdef PORTG
&PORTG,
#endif
#ifdef PORTH
&PORTH,
#endif
#ifdef PORTJ
&PORTJ,
#endif
#ifdef PORTK
&PORTK,
#endif
#ifdef PORTL
&PORTL,
#endif
};

#define MAKEPINS(x) x##0, x##1, x##2, x##3, x##4, x##5, x##6, x##7

/**
 * GPIO Pin Constants for the compiled system.
 *
 * For each port on the target, there will be a corresponding
 * port and pin definition in the form X# where 'X' is the
 * port letter and '#' is the pin number, 0-7.
 *
 * Examples are GPIO::C3 or GPIO::A0
 */
typedef enum
{
#if defined(PORTA)
MAKEPINS(A),
#endif
#if defined(PORTB)
MAKEPINS(B),
#endif
#if defined(PORTC)
MAKEPINS(C),
#endif
#if defined(PORTD)
MAKEPINS(D),
#endif
#if defined(PORTE)
MAKEPINS(E),
#endif
#if defined(PORTF)
MAKEPINS(F),
#endif
#if defined(PORTG)
MAKEPINS(G),
#endif
#if defined(PORTH)
MAKEPINS(H),
#endif
#if defined(PORTJ)
MAKEPINS(J),
#endif
#if defined(PORTK)
MAKEPINS(K),
#endif
#if defined(PORTL)
MAKEPINS(L),
#endif
NONE, ///< Do not pass into GPIO::*() functions(!!), but use for your own boundary checks
} Pin;

namespace IO
{

struct Port
{
volatile uint8_t&
operator()(uint8_t p)
{
  return *portAddr[p];
}
};

struct DDR
{
volatile uint8_t&
operator()(uint8_t p)
{
  return *(portAddr[p] - 1);
}
};

struct PinReg
{
volatile uint8_t&
operator()(uint8_t p)
{
  return *(portAddr[p] - 2);
}
};

constexpr
uint8_t
Portof(Pin pin)
{
return pin / 8;
}

namespace
{

template<Pin ... pins>
  struct PortEqualImpl;

template<Pin first, Pin ... pins>
  struct PortEqualImpl<first, pins...>
  {
    constexpr
    static uint8_t
    port()
    {
      return Portof(first) & PortEqualImpl<pins...>::port();
    }
  };

template<>
  struct PortEqualImpl<>
  {
    constexpr
    static uint8_t
    port()
    {
      return 0xFF;
    }
  };

template<Pin pin1, Pin ... pins>
  constexpr
  bool
  portequal()
  {
    return (Portof(pin1) & PortEqualImpl<pins...>::port()) == Portof(pin1);
  }

template<Pin ... pins>
  struct PinOrImpl;

template<Pin first, Pin ... pins>
  struct PinOrImpl<first, pins...>
  {
    constexpr
    static uint8_t
    port()
    {
      return _BV(first % 8) | PinOrImpl<pins...>::port();
    }
  };

template<>
  struct PinOrImpl<>
  {
    constexpr
    static uint8_t
    port()
    {
      return 0x00;
    }
  };

template<Pin first, Pin ... pins>
  constexpr
  uint8_t
  pinor()
  {
    return _BV(first % 8) | PinOrImpl<pins...>::port();
  }

struct Or
{
  uint8_t
  operator()(uint8_t a, uint8_t b)
  {
    return a | b;
  }
};

struct AndNot
{
  uint8_t
  operator()(uint8_t a, uint8_t b)
  {
    return a & ~b;
  }
};

/// ModifyPin performs TMP recursion to bundle pins
/// on the same port into an IN instruction.
template<class REG, class OP, Pin ...pins>
  struct ModifyPin;
template<class REG, class OP, Pin first, Pin ... pins>
  struct ModifyPin<REG, OP, first, pins...>
  {
    static void
    action()
    {
      OP op;
      REG reg;
      if (portequal<first, pins...>()) {
	reg(Portof(first)) = op(reg(Portof(first)), pinor<first, pins...>());
      }
      else {
	ModifyPin<REG, OP, pins...>::action();
      }
    }
  };

template<class REG, class OP>
  struct ModifyPin<REG, OP>
  {
    static void
    action()
    {
      // Got to the end of the recursion, do nothing.
    }
  };

}

template<Pin ... pins>
void
High()
{
  ModifyPin<Port, Or, pins...>::action();
}

template<Pin ... pins>
void
Low()
{
  //static_assert(portequal<pin1, pins...>(), "Cannot set pins on different ports in one call to __FUNCTION__");
  //Port(Portof(pin1)) &= ~pinor<pin1, pins...>();
  ModifyPin<Port, AndNot, pins...>::action();
}

//template<Pin ... pins>
//void
//Set(const bool ishigh)
//{
//  if(ishigh) High<pins>();
//  else Low<pins>();
//}

template<Pin ... pins>
void
Output()
{
  //static_assert(portequal<pin1, pins...>(), "Cannot set pins on different ports in one call to __FUNCTION__");
  //DDR(Portof(pin1)) |= pinor<pin1, pins...>();
  ModifyPin<DDR, Or, pins...>::action();
}

template<PortID port>
void
Output()
{
  DDR ddr;
  ddr(port) = 0xFF;
}

template<Pin ... pins>
void
Input()
{
  //static_assert(portequal<pin1, pins...>(), "Cannot set pins on different ports in one call to __FUNCTION__");
  //DDR(Portof(pin1)) &= ~pinor<pin1, pins...>();
  ModifyPin<DDR, AndNot, pins...>::action();
}

template<PortID port>
void
Input()
{
  DDR ddr;
  ddr(port) = 0x00;
}

template<Pin pin>
uint8_t
Get()
{
  PinReg P;
  return P(Portof(pin)) & (pin % 8) ? 1 : 0;
}

template<PortID port>
uint8_t
Get()
{
  PinReg P;
  return P(port);
}

template<PortID port>
void
Put(const uint8_t val)
{
  Port P;
  P(port) = val;
}



} /* namespace IO */

#endif /* AVRCPPIO_HPP_ */
